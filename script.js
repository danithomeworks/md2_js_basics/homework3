// Завдання
// Реалізувати програму на Javascript, яка знаходитиме всі числа кратні 5 (діляться на 5 без залишку) у заданому діапазоні.
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера число, яке введе користувач.

// Вивести в консолі всі числа, кратні 5, від 0 до введеного користувачем числа. Якщо таких чисел немає -
// вивести в консоль фразу "Sorry, no numbers"

// Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.

// Необов'язкове завдання підвищеної складності

// Перевірити, чи введене значення є цілим числом. Якщо ця умова не дотримується, повторювати виведення вікна на екран,
// доки не буде введено ціле число.

// Отримати два числа, m і n. Вивести в консоль усі прості числа (http://uk.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE)
// в діапазоні від m до n(менше із введених чисел буде m, більше буде n).
// Якщо хоча б одне з чисел не відповідає умовам валідації, зазначеним вище,
// вивести повідомлення про помилку і запитати обидва числа знову.

"use strict";

// Advanced task 1
function checkValidity(title) {
  let inputtedNumber;
  do {
    inputtedNumber = prompt(`${title}:`);
  } while (
    inputtedNumber === "" || // Checking for empty field
    inputtedNumber === null || // Checking for "Cancel" button
    Math.trunc(+inputtedNumber) !== +inputtedNumber // Checking round
  );
  return inputtedNumber;
}
// -----------------------------------

// Basic task
function logMultiples(inputtedNumber, multiplier) {
  if (
    inputtedNumber > -multiplier &&
    inputtedNumber < multiplier &&
    inputtedNumber != 0
  ) {
    console.log("Sorry, no numbers");
  } else {
    for (let i = 0; Math.abs(i) <= Math.abs(inputtedNumber); ) {
      console.log(i);
      inputtedNumber > 0 ? (i += multiplier) : (i -= multiplier);
    }
  }
}
// -----------------------------------

// Advanced task 2
function logPrime(start, end) {
  for (let num = start; num <= end; num++) {
    let isPrime = true;
    for (let i = 2; i < num; i++) {
      if (num % i == 0) isPrime = false;
    }
    if (isPrime) console.log(num);
  }
}
// -----------------------------------

// Basic task & Advanced task 1
console.log("Basic task and Advanced task 1 started");

let userNumber = +checkValidity("Input your number (round)");

logMultiples(userNumber, 5);
// -----------------------------------

// Advanced task 2
console.log("Advanced task 2 started");

let m = +checkValidity("Input your first number (round) for checking primes");
let n = +checkValidity("Input your second number (round) for checking primes");

m < n ? logPrime(m, n) : logPrime(n, m);
// -----------------------------------
